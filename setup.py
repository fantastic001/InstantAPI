from distutils.core import setup
setup(
  name = 'InstantAPI',
  packages = ['InstantAPI'], # this must be the same as the name above
  version = '0.1',
  description = 'Create backend easily with modular blocks',
  author = 'Stefan Nožinić',
  author_email = 'stefan@lugons.org',
  url = 'https://gitlab.com/fantastic001/InstantAPI', # use the URL to the github repo
  keywords = ["web"], # arbitrary keywords
  package_dir = {'InstantAPI': 'src/lib'},
  classifiers = [],
)
