
class CloudObject(dict):

    def __init__(self, **data):
        self.data = data 
        dict.__init__(self, data)

    def getInfo(self):
        return {
            "type": "object"
        }

    def __getattr__(self, attr):
        return self.data[attr]
    
    def toDict(self):
        return self.data 

    def fromDict(d):
        return CloudObject(**d)

