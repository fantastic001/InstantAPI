
from .. import * 

import json

class FSHandler(CloudHandler):
    
    HANDLES = "data"

    def __init__(self, root):
        """
        root: place on real filesystem where all files are held
        """
        self.root = root

    def handle(self, path, state={}):
        while path[0] == "/":
            path = path[1:]
        fpath = "%s/%s" % (self.root, path)
        if os.path.isdir(fpath):
            if os.path.isfile("%s/data.json" % fpath):
                return CloudObject.fromDict(json.loads(open("%s/data.json" % fpath).read()))
            else:
                return CloudObject(contents=os.listdir(fpath))
        what = path.split("/")[-1]
        if what == "info":
            return CloudObject.fromDict(json.loads(open(fpath).read()))
        if what == "*":
            path = "/".join(path.split("/")[:-1]) + "/"
            return CloudObject(contents=[self.handle("%s/%s" % (path, name), state) for name in os.listdir("%s/%s" % (self.root, path))])
        return CloudObject()

    def handlePut(self, path, state={}, obj=CloudObject()):
        path = "%s/%s" % (self.root, path)
        if not os.path.isdir(path):
            os.makedirs(path)
        f = open("%s/data.json" % path, "w")
        f.write(json.dumps(obj.toDict()))
        f.close()
        f = open("%s/info" % path, "w")
        f.write(json.dumps(obj.getInfo()))
        f.close()
        return obj

