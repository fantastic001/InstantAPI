
from .. import * 

import json
from base64 import b64encode, b64decode
import hmac
import hashlib

class JWTAuthHandler(CloudHandler):
    
    HANDLES = "auth"

    def __init__(self, key, users=[]):
        """
        key: byte-array representing a key
        users: List of CloudObjects where each has fields username and password
        """
        self.key = key
        self.users = users

    def verify(self, token):
        sig = token.split(".")[-1]
        data = token[:-len(sig) - 1]
        mysig = hmac.new(self.key, data.encode("utf8"), hashlib.sha256).hexdigest()
        if sig == mysig:
            return True
        else:
            return False

    def handle(self, path, state={}):
        while path[0] == "/":
            path = path[1:]
        token = path 
        sig = token.split(".")[-1]
        data = token[:-len(sig) - 1]
        mysig = hmac.new(self.key, data.encode("utf8"), hashlib.sha256).hexdigest()
        if sig == mysig:
            return CloudObject(state="ok")
        else:
            return CloudObject(state="no-ok")

    def handlePut(self, path, state={}, obj=CloudObject()):
        found = [user for user in self.users if user.username == obj.username and user.password == obj.password]
        if found == []:
            return CloudObject(error="Cannot authenticate because user not found with given parameters.")
        else:
            header = {
               "typ": "JWT",
                "alg": "HS256"
            }
            payload = {
                "user": obj.username,
            }
            data = b64encode(json.dumps(header).encode("utf8")) + b"." + b64encode(json.dumps(payload).encode("utf8"))
            sig = hmac.new(self.key, data, hashlib.sha256).hexdigest()
        return CloudObject(token = "%s.%s" % (data.decode("utf8"), sig))
