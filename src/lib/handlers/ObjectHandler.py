
from .. import * 

import json
from sqlalchemy import *


class ObjectHandler(CloudHandler):
    
    HANDLES = "objects"

    TYPE_MAPPING = {
        "string": String,
        "integer": Integer, 
        "boolean": Boolean
    }

    def __init__(self, endpoint, db_uri, table_name, columns):
        """
        endpoint: REST API endpoint to use 
        db_uri: sqlalchemy compatible db uri 
        table_name: name of the table
        columns: list of tuples, first element is column name and second is string representing type ("string", "integer", "boolean")

        NOTE: do not specify id column, it will be added automatically 
        """
        self.HANDLES = endpoint 
        self.engine = create_engine(db_uri)
        self.metadata = MetaData()
        self.table = Table(table_name, self.metadata, Column("id", Integer, primary_key=True), *[Column(x, self.TYPE_MAPPING[y]) for x,y in columns])
        self.metadataa.create_all(self.engine)
        self.conn = self.engine.connect()
    
    def row2dict(self,r):
        return {c.name: str(getattr(r, c.name)) for c in r.__table__.columns}

    def handle(self, path, state={}):
        # s = select(table.c.name, ....)
        # res = engine.execute(s)
        #for r in res:
        #    print(r) - tuple
        s = select([self.table]) 
        path = self.split(path)
        if len(path) == 1:
            i = 0
            try:
                i = int(path[0])
            except ValueError:
                return CloudObject(error="ID provided is not a number")
            finally:
                s = s.where(self.table.c.id == i)
                r = self.conn.execute(s).first()
                return CloudObject(**self.row2dict(r))
        elif len(path) == 0:
            return CloudObject(data=[self.row2dict(r) for r in self.conn.erxecute(s).fetchall()])
        else:
            return CloudObject(error="Bad params")

        # and_ in where 

    def handlePut(self, path, state={}, obj=CloudObject()):
        path = self.split(path)
        if len(path) == 0:
            ins = self.table.insert().values(**obj.toDict())
            res = self.conn.execute(ins)
            p = res.inserted_primary_key 
            if len(p) == 0:
                return CloudObject(error="oups! wrong params")
            else:
                return CloudObject(success="Object added successfully")
        elif len(path) == 1:
            i = 0
            try:
                i = int(path[0])
            except ValueError:
                return CloudObject(error="ID provided is not a number")
            finally:
                t = self.table.update().where(table.c.id == i).values(**obj.toDict())
                res = self.conn.execute(t)
                if res.rowcount > 0:
                    return CloudObject(success="Object updated")
                else:
                    return CloudObject(error="Not provided correct info")

        # TODO: table.delete().where(...)


