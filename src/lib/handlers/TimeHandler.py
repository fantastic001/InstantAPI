
from .. import * 

import datetime

class TimeHandler(CloudHandler):
    
    HANDLES = "time"

    def handle(self, path, state={}):
        return CloudObject(time=str(datetime.datetime.now()))

    def handlePut(self, path, state={}, obj=CloudObject()):
        print("Handling put")
        return CloudObject(error="not hanndling")
