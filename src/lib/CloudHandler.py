
from .CloudObject import * 

class CloudHandler(object):
    
    HANDLES = "handler"

    def handle(self, path, state={}):
        """
        Should return CloudObject
        """
        return CloudObject()

    def handlePut(self, path, state={}, obj=CloudObject()): 
        """
        Should return CloudObject and perform some operation
        """
        return CloudObject()

    def split(self, path):
        return [ k for k in os.path.normpath(path).split(os.sep) if k != ""]
