
from ..CloudServer import * 
from ..CloudObject import * 

import json

from flask import Flask, request

class HTTPServer(object):
    
    def __init__(self, server, address="0.0.0.0", port=80):
        self.server = server
        self.app = Flask(__name__)
        self.hostname = address
        self.port = port

        @self.app.route("/<path:dummy>", methods=["GET", "POST"])
        def f(dummy):
            auth = request.headers.get("Authorization", "")
            server = self.server
            if auth != "" and auth.split(" ")[0] == "JWT":
                server = self.server.withState(auth=auth.split(" ")[1])
            print(server.state)
            if request.method == "POST":
                obj = CloudObject.fromDict(request.get_json())
                obj = server.putObject(dummy, obj)
                return json.dumps(obj.toDict())
            else:
                return json.dumps(server.getObject(dummy).toDict())
        
        @self.app.route("/")
        def root():
            return json.dumps(server.getObject("/").toDict())

    def serve(self):
        self.app.run(host=self.hostname, port=self.port, debug=False)
