import json
import os 

from .CloudObject import * 

class CloudServer(object):
    
    def __init__(self, default_handler, handlers=[], state={}):
        self.handlers = handlers
        self.state = state
        self.default_handler = default_handler 

    def withState(self, **state):
        return CloudServer(self.root, self.handlers, state=dict(self.state, **state))

    def putObject(self, path, obj):
        while len(path)>0 and path[0] == "/":
            path = path[1:]
        path = os.path.normpath(path)
        handlers = [handler for handler in self.handlers if handler.HANDLES == path.split("/")[0]]
        if handlers != []:
            return handlers[0].handlePut("/".join(path.split("/")[1:]), self.state, obj)
        return self.default_handler.handlePut(path, self.state, obj)


    def getObject(self, path):
        while len(path)>0 and path[0] == "/":
            path = path[1:]
        path = os.path.normpath(path)
        handlers = [handler for handler in self.handlers if handler.HANDLES == path.split("/")[0]]
        if handlers != []:
            return handlers[0].handle("/".join(path.split("/")[1:]), self.state)
        return self.default_handler.handle(path, self.state)
