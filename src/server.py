
from lib.http import HTTPServer 
from lib.handlers import * 
from lib import CloudServer
import sys

cloud = CloudServer(FSHandler(sys.argv[1]), handlers=[
    TimeHandler(),
    JWTAuthHandler(key = b"this is some key", users=[CloudObject(username="stefan", password="12345")])
])

print("Running server on port 8080")
HTTPServer(cloud, port=8080).serve()
